use std::collections::BTreeMap;

use super::{error::Error, tag::Tag};

#[derive(Debug)]
pub enum Node {
    Tag(Tag),
    Text(String),
}

impl Node {
    pub fn parse(s: &str) -> Result<(Vec<Node>, &str), Error> {
        let mut text = String::new();
        let mut content = vec![];
        let mut rest = s;
        while rest.len() != 0 {
            let c = rest.chars().next().unwrap();
            if c == '<' {
                if text.len() != 0 {
                    content.push(Node::Text(text));
                    text = String::new();
                }
                // enclosing tag was closed: end here
                if let Some(next) = rest.chars().nth(1) {
                    if next == '/' {
                        break;
                    }
                }
                let tag;
                (tag, rest) = Tag::parse(rest)?;
                // println!("tag result: {:#?}", tag);
                // println!("tag rest: {:#?}", rest);
                content.push(Node::Tag(tag));
            } else {
                text += &rest[0..1];
            }
            rest = &rest[1..];
        }
        Ok((content, rest))
    }

    pub fn parse_document(s: &str) -> Result<Node, Error> {
        let (ns, _) = Node::parse(s)?;

        Ok(Node::Tag(Tag {
            name: ":root".to_string(),
            attributes: BTreeMap::new(),
            children: ns,
        }))
    }
}
