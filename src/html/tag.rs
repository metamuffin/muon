use std::collections::BTreeMap;

use super::{error::Error, node::Node};

#[derive(Debug)]
pub struct Tag {
    pub name: String,
    pub attributes: BTreeMap<String, Option<String>>,
    pub children: Vec<Node>,
}

impl Tag {
    pub fn parse(s: &str) -> Result<(Tag, &str), Error> {
        // println!("start tag: {:#?}", s);
        if let Some(n) = s.chars().nth(0) {
            if n != '<' {
                return Err(Error::InternalError);
            }
        }

        let (name, mut rest) = read_string_unquoted(&s[1..]);
        let name = name.to_lowercase();
        // println!("name: {:?}", name);
        // println!("name rest: {:?}", rest);
        let mut attributes = BTreeMap::new();

        while let Some(nc) = rest.chars().next() {
            match nc {
                // names starting in ! (like "<!doctype>") are self-closing
                _ if Tag::supports_self_close(name.as_str()) && nc == '>' => {
                    let tag = Tag {
                        attributes,
                        children: vec![],
                        name: String::from(name),
                    };
                    return Ok((tag, rest));
                }
                '>' => break,
                '/' => {
                    // self-closing tag
                    rest = &rest[1..];
                    if let Some(nc) = rest.chars().next() {
                        if nc != '>' {
                            return Err(Error::MalformedSelfClose);
                        }
                    }
                    let tag = Tag {
                        attributes,
                        children: vec![],
                        name: String::from(name),
                    };
                    return Ok((tag, rest));
                }
                ' ' => rest = &rest[1..],
                _ => {
                    let key;
                    (key, rest) = read_string(rest);
                    // println!("attrib: {:?} {:?}", key, rest);
                    let mut value = None;

                    if let Some(nc2) = rest.chars().next() {
                        if nc2 == '=' {
                            let value_s;
                            (value_s, rest) = read_string(&rest[1..]);
                            value = Some(value_s);
                        }
                    }
                    attributes.insert(String::from(key), value.map(String::from));
                }
            }
        }
        rest = &rest[1..];
        // println!("content start: {:#?}", rest);

        let children;
        (children, rest) = Node::parse(rest)?;

        let close_tag = format!("</{}>", name);
        // println!("{:#?}", rest);
        if !rest.starts_with(close_tag.as_str()) {
            return Err(Error::UnknownClosingTag(
                rest.split_once(">")
                    .unwrap_or(("## EOF ##", ""))
                    .0
                    .to_string(),
            ));
        }
        rest = &rest[(close_tag.len() - 1)..];

        let tag = Tag {
            attributes,
            children,
            name: String::from(name),
        };
        Ok((tag, rest))
    }

    pub fn supports_self_close(name: &str) -> bool {
        match name {
            "!doctype" => true,
            "hr" | "br" | "meta" | "link" => true,
            _ => false,
        }
    }
}

pub fn read_string(s: &str) -> (&str, &str) {
    if let Some(fc) = s.chars().nth(0) {
        if fc == '"' {
            read_string_quoted(s)
        } else {
            read_string_unquoted(s)
        }
    } else {
        ("", "")
    }
}

pub fn read_string_quoted(s: &str) -> (&str, &str) {
    // TODO escape seq
    s[1..].split_once("\"").unwrap_or((s, ""))
}
pub fn read_string_unquoted(s: &str) -> (&str, &str) {
    if let Some(idx) = s.find(&['=', ' ', '>']) {
        (&s[0..idx], &s[idx..])
    } else {
        (s, "")
    }
}
