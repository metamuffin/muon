#[derive(Debug)]
pub enum Error {
    InternalError,
    UnknownClosingTag(String),
    MalformedSelfClose,
}
