use std::{
    io::{Read, Write},
    net::TcpStream,
};

use super::{error::Error, request::Request, response::Response};

pub struct Client {}

impl Client {
    pub fn new() -> Self {
        Self {}
    }

    pub fn fetch(&mut self, mut request: Request) -> Result<Response, Error> {
        let protocol = request
            .url
            .protocol
            .clone()
            .ok_or(Error::UnknownProtocol("### none ###".to_string()))?;
        request.url.port = Some(
            request
                .url
                .infer_port()
                .ok_or(Error::UnknownProtocol(protocol))?,
        );
        let host = request.url.host().ok_or(Error::RelativeUrl)?;
        request.headers.set("host", host.clone());
        if let Some(body) = &request.body {
            request
                .headers
                .set("content-length", format!("{}", body.len()));
        }

        // TODO keep track of open connections in the struct for reuse
        let mut sock = TcpStream::connect(host).map_err(|e| Error::SocketError(e))?;

        let r = request.serialize();

        sock.write_fmt(format_args!("{}", r))
            .map_err(|e| Error::SocketError(e))?;

        let mut header_buf = String::new();

        loop {
            let mut char_buf = vec![0u8; 1];
            sock.read(&mut char_buf[..])
                .map_err(|e| Error::SocketError(e))?;
            header_buf += unsafe { String::from_utf8_unchecked(char_buf).as_str() };
            if header_buf.ends_with("\r\n\r\n") {
                // end of headers reached
                break;
            }
        }

        let mut response = Response::parse(&header_buf)?;

        if let Some(len_str) = response.headers.get("content-length") {
            let len = len_str.parse::<usize>().map_err(|_| {
                Error::HeaderValueInvalid("content-length".to_string(), len_str.to_string())
            })?;
            let mut body_buf = String::new();
            for _ in 0..len {
                let mut char_buf = vec![0u8; 1];
                sock.read(&mut char_buf[..])
                    .map_err(|e| Error::SocketError(e))?;
                body_buf += unsafe { String::from_utf8_unchecked(char_buf).as_str() };
            }
            response.body = Some(body_buf);
        } else if let Some(enc) = response.headers.get("transfer-encoding") {
            if enc == "chunked" {
                let mut body_buf = String::new();
                loop {
                    let mut chunk_len_buf = String::new();
                    while !chunk_len_buf.ends_with("\r\n") {
                        let mut char_buf = vec![0u8; 1];
                        sock.read(&mut char_buf[..])
                            .map_err(|e| Error::SocketError(e))?;
                        chunk_len_buf += unsafe { String::from_utf8_unchecked(char_buf).as_str() };
                    }
                    // TODO
                    let chunk_len = usize::from_str_radix(chunk_len_buf.trim_end(), 16).unwrap();
                    for _ in 0..chunk_len {
                        let mut char_buf = vec![0u8; 1];
                        sock.read(&mut char_buf[..])
                            .map_err(|e| Error::SocketError(e))?;
                        body_buf += unsafe { String::from_utf8_unchecked(char_buf).as_str() };
                    }
                    let mut char_buf = vec![0u8; 1];
                    sock.read(&mut char_buf[..])
                        .map_err(|e| Error::SocketError(e))?;
                    sock.read(&mut char_buf[..])
                        .map_err(|e| Error::SocketError(e))?;
                    if chunk_len == 0 {
                        break;
                    }
                }
                response.body = Some(body_buf);
            } else {
                return Err(Error::InvalidTransferEncoding(String::from(enc)));
            }
        }

        return Ok(response);
    }
}
