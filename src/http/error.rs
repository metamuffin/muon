use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    HeaderInvalid(String),
    HeaderValueInvalid(String, String),
    ResponseMalformed,
    SocketError(IOError),
    RelativeUrl,
    UnknownProtocol(String),
    InvalidTransferEncoding(String),
}
