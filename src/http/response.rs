use super::{error::Error, headers::Headers, Status};

#[derive(Debug, Clone)]
pub struct Response {
    pub status: Status,
    pub body: Option<String>,
    pub headers: Headers,
}

impl Response {
    pub fn parse(s: &str) -> Result<Self, Error> {
        let (first_line, header_lines) = s.split_once("\r\n").ok_or(Error::ResponseMalformed)?;
        let (_version, rest) = first_line.split_once(" ").ok_or(Error::ResponseMalformed)?;
        let (status_code, _status_str) = rest.split_once(" ").ok_or(Error::ResponseMalformed)?;
        let status = Status::parse(status_code);
        let headers = Headers::parse(header_lines)?;

        Ok(Self {
            body: None,
            headers,
            status,
        })
    }
}
