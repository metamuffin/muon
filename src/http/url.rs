use super::query::Query;
use std::fmt::{Debug, Display};

#[derive(Clone)]
pub struct Url {
    pub protocol: Option<String>,
    pub hostname: Option<String>,
    pub port: Option<u16>,
    pub path: String,
    pub query: Option<Query>,
    pub username: Option<String>,
    pub password: Option<String>,
    pub section: Option<String>,
}

impl Debug for Url {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self))
    }
}
impl Display for Url {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.serialize()))
    }
}

impl Default for Url {
    fn default() -> Self {
        Self {
            protocol: Default::default(),
            hostname: Default::default(),
            port: Default::default(),
            path: Default::default(),
            query: Default::default(),
            username: Default::default(),
            password: Default::default(),
            section: Default::default(),
        }
    }
}

impl Url {
    pub fn path_urlencoded(&self) -> String {
        return self.path.clone(); // TODO
    }

    pub fn host(&self) -> Option<String> {
        self.hostname.as_ref().map(|h| match self.port {
            Some(p) => format!("{}:{}", h, p),
            None => h.clone(),
        })
    }
    pub fn infer_port(&self) -> Option<u16> {
        self.protocol
            .as_ref()
            .map(|p| match p.as_str() {
                "http" => Some(80),
                "https" => Some(443),
                _ => None,
            })
            .flatten()
    }

    pub fn serialize(&self) -> String {
        match self.host() {
            Some(h) => format!(
                "{}://{}{}",
                self.protocol.as_ref().unwrap_or(&"".to_string()),
                h,
                self.rest_urlencoded()
            ),
            None => self.rest_urlencoded(),
        }
    }

    pub fn rest_urlencoded(&self) -> String {
        self.path_urlencoded()
    }

    pub fn parse(s: &str) -> Self {
        // TODO everything
        let (protocol, rest) = s.split_once("://").unwrap();
        let (host, rest) = rest.split_once("/").unwrap();

        Self {
            port: None,
            hostname: Some(String::from(host)),
            password: None,
            section: None,
            username: None,
            path: format!("/{}", rest),
            protocol: Some(String::from(protocol)),
            query: None,
        }
    }
}
