pub mod client;
pub mod headers;
pub mod query;
pub mod request;
pub mod response;
pub mod url;
pub mod error;

#[derive(Debug, Clone)]
pub enum Method {
    GET,
    POST,
    PUT,
    OPTIONS,
    UPDATE,
    DELETE,
    TRACE,
    CONNECT,
}

#[derive(Debug, Clone)]
pub enum Status {
    Continue,
    SwitchingProtocols,
    Ok,
    Created,
    Accepted,
    NonAuthoritativeInformation,
    NoContent,
    ResetContent,
    PartialContent,
    MultipleChoices,
    MovedPermanently,
    Found,
    SeeOther,
    NotModified,
    UseProxy,
    Unused,
    TemporaryRedirect,
    BadRequest,
    Unauthorized,
    PaymentRequired,
    Forbidden,
    NotFound,
    MethodNotAllowed,
    NotAcceptable,
    ProxyAuthenticationRequired,
    RequestTimeout,
    Conflict,
    Gone,
    LengthRequired,
    PreconditionFailed,
    RequestEntityTooLarge,
    RequestURITooLong,
    UnsupportedMediaType,
    RequestedRangeNotSatisfiable,
    ExpectationFailed,
    Other(u16),
}

impl Status {
    pub fn parse(s: &str) -> Self {
        match s.parse::<u16>().unwrap() {
            100 => Status::Continue,
            101 => Status::SwitchingProtocols,
            200 => Status::Ok,
            201 => Status::Created,
            202 => Status::Accepted,
            203 => Status::NonAuthoritativeInformation,
            204 => Status::NoContent,
            205 => Status::ResetContent,
            206 => Status::PartialContent,
            300 => Status::MultipleChoices,
            301 => Status::MovedPermanently,
            302 => Status::Found,
            303 => Status::SeeOther,
            304 => Status::NotModified,
            305 => Status::UseProxy,
            306 => Status::Unused,
            307 => Status::TemporaryRedirect,
            400 => Status::BadRequest,
            401 => Status::Unauthorized,
            402 => Status::PaymentRequired,
            403 => Status::Forbidden,
            404 => Status::NotFound,
            405 => Status::MethodNotAllowed,
            406 => Status::NotAcceptable,
            407 => Status::ProxyAuthenticationRequired,
            408 => Status::RequestTimeout,
            409 => Status::Conflict,
            410 => Status::Gone,
            411 => Status::LengthRequired,
            412 => Status::PreconditionFailed,
            413 => Status::RequestEntityTooLarge,
            414 => Status::RequestURITooLong,
            415 => Status::UnsupportedMediaType,
            416 => Status::RequestedRangeNotSatisfiable,
            417 => Status::ExpectationFailed,
            n => Status::Other(n),
        }
    }
}

impl Method {
    pub fn serialize(&self) -> String {
        match self {
            Method::GET => String::from("GET"),
            Method::POST => String::from("POST"),
            Method::PUT => String::from("PUT"),
            Method::OPTIONS => String::from("OPTIONS"),
            Method::UPDATE => String::from("UPDATE"),
            Method::DELETE => String::from("DELETE"),
            Method::TRACE => String::from("TRACE"),
            Method::CONNECT => String::from("CONNECT"),
        }
    }
}
