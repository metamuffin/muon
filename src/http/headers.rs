use super::error::Error;
use std::collections::BTreeMap;

#[derive(Debug, Clone)]
pub struct Headers {
    map: BTreeMap<String, String>,
}

impl Headers {
    pub fn new() -> Self {
        return Self {
            map: BTreeMap::new(),
        };
    }
    pub fn get(&self, s: &str) -> Option<&String> {
        self.map.get(&s.to_lowercase())
    }
    pub fn set(&mut self, k: &str, v: String) {
        self.map.insert(k.to_string(), v);
    }
    pub fn serialize(&self) -> String {
        self.map
            .iter()
            .map(|(key, value)| format!("{}: {}\r\n", key, value))
            .collect::<Vec<_>>()
            .concat()
    }
    pub fn parse(s: &str) -> Result<Self, Error> {
        // TODO: look up if multi-line values exists in http (arpa internet text messages)
        let mut map = BTreeMap::new();
        for line in s.split("\r\n") {
            if line == "" {
                continue;
            }
            // TODO: check if the space is required
            let (key, value) = line
                .split_once(": ")
                .ok_or(Error::HeaderInvalid(line.to_string()))?;
            map.insert(String::from(key).to_lowercase(), String::from(value));
        }
        Ok(Self { map })
    }
}
