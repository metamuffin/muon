use super::{headers::Headers, url::Url, Method};

#[derive(Debug, Clone)]
pub struct Request {
    pub url: Url,
    pub method: Method,
    pub headers: Headers,
    pub body: Option<String>,
}
impl Request {
    pub fn new() -> Self {
        Self {
            body: None,
            headers: Headers::new(),
            method: Method::GET,
            url: Url::default(),
        }
    }
    pub fn serialize(&self) -> String {
        format!(
            "{} {} {}\r\n{}\r\n{}",
            self.method.serialize(),
            self.url.path_urlencoded(),
            "HTTP/1.1",
            self.headers.serialize(),
            self.body.as_ref().unwrap_or(&String::from(""))
        )
    }
}
