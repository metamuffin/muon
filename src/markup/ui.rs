use crate::ui::{
    buffer::Image,
    list::UList,
    style::{Color, Style},
    text::UText,
    vec2::Vec2,
    Draw,
};

use super::MarkupNode;

#[derive(Debug, Clone, Copy)]
struct MarkupNodeToUIContext {
    style: Style,
}

impl MarkupNode {
    pub fn ui(&self) -> Box<dyn Draw> {
        self.ui_p(MarkupNodeToUIContext {
            style: Style::default(),
        })
    }
    fn ui_p(&self, c: MarkupNodeToUIContext) -> Box<dyn Draw> {
        match self {
            MarkupNode::Empty => Box::new(()),
            MarkupNode::Container(e) => Box::new(UList::new(e.iter().map(|e| e.ui_p(c)).collect())),
            MarkupNode::InlineContainer(e) => {
                Box::new(UList::new(e.iter().map(|e| e.ui_p(c)).collect()))
            }
            MarkupNode::Text(s) => Box::new(UText::new(String::from(s)).style(c.style)),
            MarkupNode::Bold(e) => e.ui_p(c),
            MarkupNode::Italic(e) => e.ui_p(c),
            MarkupNode::Underline(e) => e.ui_p(c),
            MarkupNode::Header(_, e) => e.ui_p(MarkupNodeToUIContext {
                style: Style::new().bold(true).underline(true),
                ..c
            }),
            MarkupNode::Anchor { inner, href } => Box::new(UList::new(vec![
                inner.ui_p(MarkupNodeToUIContext {
                    style: Style::new().foreground(Some(Color(100, 100, 255))),
                    ..c
                }),
                Box::new(
                    UText::new(format!("{}", href))
                        .style(Style::new().foreground(Some(Color(100, 100, 255)))),
                ),
            ])),
            _ => Box::new(UText::new(format!("TODO: {:?}", self))),
        }
    }
}

impl Draw for MarkupNode {
    fn size(&self, max_size: Vec2) -> Vec2 {
        self.ui().size(max_size)
    }
    fn draw(&self, size: Vec2) -> Image {
        self.ui().draw(size)
    }
}
