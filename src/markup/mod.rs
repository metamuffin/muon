pub mod ui;

use crate::html::node::Node;
use crate::html::tag::Tag;
use crate::http::{url::Url, Method};

#[derive(Debug, Default, Clone)]
pub struct MarkupStyle {
    pub bold: bool,
    pub italic: bool,
    pub strike: bool,
    pub underline: bool,
    pub headline: Option<usize>,
}

#[derive(Debug, Clone)]
pub enum MarkupNode {
    Empty,
    Container(Vec<MarkupNode>),
    InlineContainer(Vec<MarkupNode>),
    Text(String),
    Bold(Box<MarkupNode>),
    Italic(Box<MarkupNode>),
    Underline(Box<MarkupNode>),
    Header(usize, Box<MarkupNode>),
    Anchor {
        href: Url,
        inner: Box<MarkupNode>,
    },
    List {
        items: Vec<MarkupNode>,
        ordered: bool,
    },
    Form {
        inner: Vec<MarkupNode>,
        method: Method,
        action: Url,
    },
    Input {
        name: String,
        value: String,
    },
}

impl MarkupNode {
    pub fn load_document(hn: &Node) -> Self {
        MarkupNode::from_html_node(hn).strip()
    }

    pub fn strip(self) -> Self {
        let oc = |ns: Vec<MarkupNode>, wf: fn(Vec<MarkupNode>) -> MarkupNode| {
            let r = ns
                .into_iter()
                .map(|n| n.strip())
                .filter(|n| match n {
                    MarkupNode::Empty => false,
                    _ => true,
                })
                .collect::<Vec<_>>();
            match r.len() {
                0 => MarkupNode::Empty,
                1 => r[0].clone(),
                _ => wf(r),
            }
        };
        match self {
            MarkupNode::Empty => self,
            MarkupNode::Container(ns) => oc(ns, |ns| MarkupNode::Container(ns)),
            MarkupNode::InlineContainer(ns) => oc(ns, |ns| MarkupNode::InlineContainer(ns)),
            MarkupNode::Text(t) => match t.len() {
                0 => MarkupNode::Empty,
                _ => MarkupNode::Text(t),
            },
            MarkupNode::Bold(i) => MarkupNode::Bold(Box::new(i.strip())),
            MarkupNode::Italic(i) => MarkupNode::Italic(Box::new(i.strip())),
            MarkupNode::Underline(i) => MarkupNode::Underline(Box::new(i.strip())),
            MarkupNode::Header(l, i) => MarkupNode::Header(l, Box::new(i.strip())),
            _ => self, // TODO at lease strip the inner parts of the others
        }
    }

    pub fn from_html_node(hn: &Node) -> Self {
        match hn {
            Node::Tag(t) => MarkupNode::from_html_tag(t),
            Node::Text(s) => {
                MarkupNode::Text(s.trim().replace("\n", " ").replace("\t", " ").to_string())
            }
        }
    }
    pub fn from_html_tag(ht: &Tag) -> Self {
        let inner = || {
            ht.children
                .iter()
                .map(|n| MarkupNode::from_html_node(n))
                .collect()
        };
        match ht.name.to_lowercase().as_str() {
            "h1" | "h2" | "h3" | "h4" | "h5" | "h6" => MarkupNode::Header(
                format!("{}", ht.name.chars().nth(1).unwrap())
                    .parse::<usize>()
                    .unwrap(),
                Box::new(MarkupNode::Container(inner())),
            ),
            "a" => MarkupNode::Anchor {
                inner: Box::new(MarkupNode::InlineContainer(inner())),
                href: Url::parse(
                    ht.attributes
                        .get("href")
                        .unwrap_or(&None)
                        .as_ref()
                        .unwrap_or(&String::from("#"))
                        .as_str(),
                ),
            },
            "p" => MarkupNode::InlineContainer(inner()),
            "head" | "script" | "style" => MarkupNode::Empty,
            _ => MarkupNode::Container(inner()),
        }
    }
}
