#![feature(destructuring_assignment)]
#![feature(exclusive_range_pattern)]

use ui::{border::UBorder, io::TerminalRenderer};

use crate::{
    html::node::Node,
    http::{client::Client, headers::Headers, request::Request, url::Url, Method},
    markup::MarkupNode,
};

pub mod html;
pub mod http;
pub mod markup;
pub mod ui;

fn main() {
    let args = std::env::args().collect::<Vec<_>>();

    let mut client = Client::new();

    let req = Request {
        url: Url::parse(args[1].as_str()),
        method: Method::GET,
        headers: Headers::new(),
        body: None,
    };
    // println!("{:#?}", req);


    let res = client.fetch(req).unwrap();
    // println!("{:#?}", res);

    let body = res.clone().body.unwrap();
    let html = Node::parse_document(body.as_str()).unwrap();
    // println!("{:#?}", html);

    let markup = MarkupNode::load_document(&html);
    // println!("{:#?}", markup);

    let mut renderer = TerminalRenderer::new(Box::new(UBorder::new(markup.ui())));
    renderer.redraw();

}
