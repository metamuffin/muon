
#[macro_export]
macro_rules! impl_setter {
    ($n: ident: $t:ty) => {
        pub fn $n(self, $n: $t) -> Self {
            Self { $n, ..self }
        }
    };
}