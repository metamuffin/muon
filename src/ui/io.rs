use crate::ui::vec2::xy;
use super::Draw;


pub struct TerminalRenderer {
    root: Box<dyn Draw>,
}

impl TerminalRenderer {
    pub fn new(root: Box<dyn Draw>) -> Self {
        TerminalRenderer { root }
    }

    pub fn run(&mut self) {
        loop {
            self.redraw();
            std::thread::sleep(std::time::Duration::from_millis(1000 / 10));
        }
    }
    pub fn redraw(&mut self) {
        let size = self.root.size(xy(100, 30));
        let i = self.root.draw(size);
        println!("{}", i);
        println!("");
    }
}
