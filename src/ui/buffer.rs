use std::fmt::Display;

use super::{style::Style, Vec2};

#[derive(Debug, Clone)]
pub struct Image {
    pub size: Vec2,
    chars: Vec<(Vec2, char, Style)>,
}

impl Image {
    pub fn new(size: Vec2) -> Self {
        Image {
            size,
            chars: vec![],
        }
    }
    pub fn ch(&mut self, pos: Vec2, c: char, style: Style) {
        if pos.x < self.size.x || pos.y < self.size.y {
            self.chars.push((pos, c, style))
        }
    }
    pub fn str(&mut self, pos: Vec2, s: &String, style: Style) {
        s.chars()
            .enumerate()
            .for_each(|(i, c)| self.ch(pos + Vec2::x(i as i64), c, style));
    }
    pub fn im(&mut self, pos: Vec2, image: &Image) {
        self.chars
            .extend(image.chars.iter().map(|(p, c, s)| (*p + pos, *c, *s)));
    }
}

impl Display for Image {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("\x1b[2J")?;
        for (p, c, s) in &self.chars {
            f.write_fmt(format_args!(
                "\x1b[{},{}H{}",
                p.y + 1,
                p.x + 1,
                s.escape(format!("{}", c).as_str()),
            ))?;
        }
        Ok(())
    }
}
