use self::{
    buffer::Image,
    vec2::{xy, Vec2},
};

// use std::sync::{Arc, Mutex};

// use self::buffer::DrawBuffer;

pub mod border;
pub mod buffer;
pub mod io;
pub mod list;
pub mod style;
pub mod text;
pub mod util;
pub mod vec2;

pub trait Draw {
    fn size(&self, max_size: Vec2) -> Vec2;
    fn draw(&self, size: Vec2) -> Image;
}

impl Draw for () {
    fn size(&self, _max_size: Vec2) -> Vec2 {
        xy(0, 0)
    }

    fn draw(&self, size: Vec2) -> Image {
        Image::new(size)
    }
}
