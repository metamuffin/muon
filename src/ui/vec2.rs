#[derive(Debug, Clone, Copy)]
pub struct Vec2 {
    pub x: i64,
    pub y: i64,
}
impl Vec2 {
    pub fn x(x: i64) -> Self {
        Self { x, y: 0 }
    }
    pub fn y(y: i64) -> Self {
        Self { y, x: 0 }
    }
    pub fn xy(x: i64, y: i64) -> Self {
        Self { x, y }
    }
}
pub fn x0(x: i64) -> Vec2 {
    Vec2 { x, y: 0 }
}
pub fn y0(y: i64) -> Vec2 {
    Vec2 { y, x: 0 }
}
pub fn xy(x: i64, y: i64) -> Vec2 {
    Vec2 { x, y }
}

impl std::ops::Add for Vec2 {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
impl std::ops::Sub for Vec2 {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}
impl std::ops::Mul for Vec2 {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x * rhs.x,
            y: self.y * rhs.y,
        }
    }
}
