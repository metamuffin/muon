use super::{
    buffer::Image,
    style::Style,
    vec2::{xy, Vec2},
    Draw,
};

pub struct UText {
    pub text: String,
    pub style: Style,
}
impl UText {
    pub fn new(text: String) -> Self {
        Self {
            text,
            style: Default::default(),
        }
    }
    pub fn style(self, style: Style) -> Self {
        Self { style, ..self }
    }
    pub fn text(self, text: String) -> Self {
        Self { text, ..self }
    }
}
impl Draw for UText {
    fn size(&self, max_size: Vec2) -> Vec2 {
        let l = self.text.len() as i64;
        let h = (l / max_size.x + if l % max_size.x != 0 { 1 } else { 0 }).min(max_size.y);
        xy(l.min(max_size.x), h)
    }

    fn draw(&self, size: Vec2) -> Image {
        let mut i = Image::new(size);
        let mut p = xy(0, 0);
        for c in self.text.chars() {
            i.ch(p, c, self.style);
            p.x += 1;
            if p.x >= size.x {
                p.x = 0;
                p.y += 1;
            }
        }

        return i;
    }
}
