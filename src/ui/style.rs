use crate::impl_setter;

#[derive(Debug, Clone, Copy, Default)]
pub struct Color(pub u8, pub u8, pub u8);
#[derive(Debug, Clone, Copy, Default)]

pub struct Style {
    pub bold: bool,
    pub underline: bool,
    pub italic: bool,
    pub foreground: Option<Color>,
    pub background: Option<Color>,
}

impl Style {
    pub fn new() -> Self {
        Default::default()
    }
    pub fn escape(&self, s: &str) -> String {
        format!("{}{}{}", self.escape_start(), s, self.escape_end())
    }
    pub fn escape_start(&self) -> String {
        format!(
            "{}{}",
            if self.bold { "\x1b[1m" } else { "" },
            if let Some(c) = self.foreground {
                format!("\x1b[38;2;{};{};{}m", c.0, c.1, c.2)
            } else {
                "".to_string()
            }
        ) // TODO
    }
    pub fn escape_end(&self) -> String {
        format!("\x1b[0m")
    }
}

impl Style {
    impl_setter!(bold: bool);
    impl_setter!(underline: bool);
    impl_setter!(italic: bool);
    impl_setter!(foreground: Option<Color>);
    impl_setter!(background: Option<Color>);
}
