use super::{
    buffer::Image,
    style::Style,
    vec2::{xy, Vec2},
    Draw,
};

const BORDER_HORIZONTAL: char = '─';
const BORDER_VERTICAL: char = '│';
const BORDER_TOP_LEFT: char = '┌';
const BORDER_TOP_RIGHT: char = '┐';
const BORDER_BOTTOM_LEFT: char = '└';
const BORDER_BOTTOM_RIGHT: char = '┘';

pub struct UBorder {
    inner: Box<dyn Draw>,
    style: Style,
}

impl UBorder {
    pub fn new(inner: Box<dyn Draw>) -> Self {
        Self {
            inner,
            style: Default::default(),
        }
    }
}

impl Draw for UBorder {
    fn size(&self, max_size: Vec2) -> Vec2 {
        self.inner.size(max_size - xy(2, 2)) + xy(2, 2)
    }

    fn draw(&self, size: Vec2) -> Image {
        let ci = self.inner.draw(size - xy(2, 2));
        let w = size.x;
        let h = size.y;
        let mut i = Image::new(size);
        i.im(Vec2::xy(1, 1), &ci);

        i.ch(xy(0, 0), BORDER_TOP_LEFT, self.style);
        i.ch(xy(w - 1, 0), BORDER_TOP_RIGHT, self.style);
        i.ch(xy(0, h - 1), BORDER_BOTTOM_LEFT, self.style);
        i.ch(xy(w - 1, h - 1), BORDER_BOTTOM_RIGHT, self.style);
        for x in 1..w - 1 {
            i.ch(xy(x, 0), BORDER_HORIZONTAL, self.style);
            i.ch(xy(x, h - 1), BORDER_HORIZONTAL, self.style);
        }
        for y in 1..h - 1 {
            i.ch(xy(0, y), BORDER_VERTICAL, self.style);
            i.ch(xy(w - 1, y), BORDER_VERTICAL, self.style);
        }
        i
    }
}
