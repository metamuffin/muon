use super::{
    buffer::Image,
    vec2::{xy, Vec2},
    Draw,
};

pub struct UList(Vec<Box<dyn Draw>>);

impl UList {
    pub fn new(a: Vec<Box<dyn Draw>>) -> Self {
        Self(a)
    }
}

impl Draw for UList {
    fn size(&self, max_size: Vec2) -> Vec2 {
        let mut t = xy(0, 0);
        println!("{:?}", max_size);
        for e in &self.0 {
            let ms = xy(max_size.x, max_size.y - t.y);
            let s = e.size(ms);
            t.y += s.y;
            t.x = t.x.max(s.x)
        }
        t
    }
    
    fn draw(&self, size: Vec2) -> Image {
        let mut off_y = 0;
        let mut i = Image::new(size);
        println!("k {:?}", size);
        for e in &self.0 {
            let ms = xy(size.x, size.y - off_y);
            let s = e.size(ms);
            i.im(xy(0, off_y), &e.draw(s));
            off_y += s.y;
        }
        return i;
    }
}
